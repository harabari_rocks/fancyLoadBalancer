package balancers

import (
	bs "fancyLoadBalancer/backendSelectors"
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"
	"fancyLoadBalancer/myutil"
)

type simpleBalancer struct {
	name  string
	bs    bs.BackendSelector
	proxy *httputil.ReverseProxy
}

func (sb *simpleBalancer) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	defer func() {
		if err := recover(); err != nil {
			switch err.(type) {
			case error:
				rw.WriteHeader(503)
				// TODO: Create a custom 503 html page
				req.Body.Close()
			default:
				panic(err)
			}
		}
	}()
	sb.proxy.ServeHTTP(rw, req)
}

func newSimpleBalancer(conf *BalancerConfig) (Balancer, error) {
	log.Printf(myutil.Time() + ": Creating new HTTP balancer %s", conf.Name)
	proxy := &httputil.ReverseProxy{Director: func(req *http.Request) {
		backend, err := conf.Backend.Select()
		if err != nil {
			log.Printf(myutil.Time() + ": no backend for client %s", req.RemoteAddr)
			panic(fmt.Errorf("failed to obtain a backend"))
		}
		log.Printf(myutil.Time() + ": Balancer %s redirecting %s to %s", conf.Name, req.RemoteAddr, backend)
		req.URL.Host = backend.String()
	}}
	return &simpleBalancer{conf.Name, conf.Backend, proxy}, nil
}

func (sb *simpleBalancer) UpdateBackends(backends []bs.Backend) {
	log.Printf(myutil.Time() + ": Updating %s balancer", sb.name)
	sb.bs.Update(backends)
}

func init() {
	Factories["SimpleBalancer"] = newSimpleBalancer
}
