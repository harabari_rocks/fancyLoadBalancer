# Work plan

```mermaid
graph LR;
    Endpoint-->Router;
    Router-->Balancer;
    Balancer-->Backend;
```
