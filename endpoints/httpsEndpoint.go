package endpoints

import (
	"crypto/tls"
	"fancyLoadBalancer/myutil"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/dyson/certman"
)

type httpsEndpoint struct {
	name     string
	bind     string
	CertFile string
	KeyFile  string
	server   http.Handler
}

func newHTTPSEndpoint(conf *EndpointConfig) (Endpoint, error) {
	if len(conf.KeyFile) == 0 {
		return nil, (fmt.Errorf("missing keye file for https server"))
	}
	if len(conf.CertFile) == 0 {
		return nil, (fmt.Errorf("missing keye file for https server"))
	}
	log.Printf(myutil.Time()+": Creating HTTPS endpoint %s", conf.Name)
	if conf.Router == nil {
		return nil, fmt.Errorf("missing router object")
	}
	ha := &httpsHeaderAdder{conf.Router}

	return &httpsEndpoint{conf.Name, conf.Bind, conf.CertFile, conf.KeyFile, ha}, nil
}

func init() {
	Factories["HTTPS"] = newHTTPSEndpoint
}

// Added headers for downgraded connection
type httpsHeaderAdder struct {
	server http.Handler
}

func (dg httpsHeaderAdder) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	req.URL.Scheme = "http"
	req.Header.Add("X-Forwarded-Proto", "https")
	req.Header.Add("X-Real-IP", myutil.RealIP(req))
	dg.server.ServeHTTP(rw, req)
}

func (hts *httpsEndpoint) Listen() {
	logger := log.New(os.Stdout, "", log.LstdFlags)

	logger.Printf(myutil.Time()+": Endpoint %s listening on %s", hts.name, hts.bind)

	cm, err := certman.New(hts.CertFile, hts.KeyFile)
	if err != nil {
		logger.Println(err)
	}
	cm.Logger(logger)
	if err := cm.Watch(); err != nil {
		logger.Println(err)
	}

	s := &http.Server{
		Addr: hts.bind,
		TLSConfig: &tls.Config{
			GetCertificate: cm.GetCertificate,
		},
		Handler: hts.server,
	}

	if err := s.ListenAndServeTLS("", ""); err != nil {
		log.Fatalf(myutil.Time()+": failed to bind httpsEndpoint to %s: %s", hts.bind, err.Error())
	}
}
