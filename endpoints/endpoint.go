// Package endpoints provides that accept traffic
package endpoints

import "net/http"

// EndpointConfig contains information necessay to create a Endpoint
type EndpointConfig struct {
	Name, EndpointType,
	Bind, CertFile, KeyFile string
	Router http.Handler
}

// Endpoint will balance request between different backends
type Endpoint interface {
	Listen()
}

// EndpointFactory will produce a Endpoint of a giver variety.
// `selectorFactory` is a factory that will be used to create appropriate BackendSelector.
type EndpointFactory func(*EndpointConfig) (Endpoint, error)

// Factories is a map of factories that let you create
// a Endpoint of a giver variety.
var Factories map[string]EndpointFactory

func init() {
	Factories = make(map[string]EndpointFactory, 0)
}

// Build build a balancer from a valid configuration
func Build(conf *EndpointConfig) (Endpoint, error) {
	return Factories[conf.EndpointType](conf)
}
