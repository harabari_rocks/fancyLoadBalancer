// Package backendselectors provides BackendSelectors that can be used
// in a load balancer.
package backendselectors

// TODO: instead of using raw string, use ip adresses or custom wrapper

// Backend is a metaclass for backends
type Backend string

func (b *Backend) String() string {
	return string(*b)
}

type BackendSelectorConfig struct {
	Type     string
	Backends []Backend
}

// Strings2Backends converts a slice of strings into a slice of backends
func Strings2Backends(ss []string) []Backend {
	h := make([]Backend, len(ss))
	for i, v := range ss {
		h[i] = Backend(v)
	}
	return h
}

// BackendUpdater is a function type desined to update backend selectors
type BackendUpdater func(BackendSelector)

// BackendSelector will select on backend from availability list.
type BackendSelector interface {

	// TODO: it may be interesting to remeber which client accessed which backend
	// and route them to same way on next request

	// Selects a backend.
	Select() (Backend, error)
	// Copy  makes a copy of itself
	Copy() BackendSelector
	// Len returns the number of registered backends
	Len() int
	// Update updates the list of backends
	Update([]Backend)
}

// TODO: instead of list of backend, create a BackendProvider interface,
// that could periodically update itself

// BackendSelectorFactory will produce a backendSelector of a giver variety.
// `backends` is a slice of backend adresses as strings with format "backend:port".
type BackendSelectorFactory func(*BackendSelectorConfig) (BackendSelector, error)

// Factories is a map of factories that let you create
// a BackendSelector of a giver variety.
var Factories map[string]BackendSelectorFactory

func init() {
	Factories = make(map[string]BackendSelectorFactory, 0)
}

// Build build a BackendSelector given type string and list of backends
func Build(bsc *BackendSelectorConfig) (BackendSelector, error) {
	return Factories[bsc.Type](bsc)
}
