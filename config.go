package main

import (
	"io/ioutil"

	yaml "gopkg.in/yaml.v2"
)

// Service is an objectet containing a description of a service
type Service struct {
	Discovery      string   `yaml:"discovery"`
	AutoUpdate     bool     `yaml:"autoUpdate"`
	UpdateInterval int      `yaml:"updateInterval"`
	Bstype         string   `yaml:"backndSelectorType"`
	StripPrefix    bool     `yaml:"stripPrefix"`
	Backends       []string `yaml:"backends,flow"`
}

// YamlConfig is go object mirroring the structure of configuration file
type YamlConfig struct {
	Name         string             `yaml:"name"`
	EndpointType string             `yaml:"endpointType"`
	Bind         string             `yaml:"bind"`
	CertFile     string             `yaml:"certificate"`
	KeyFile      string             `yaml:"key"`
	Routes       map[string]string  `yaml:"routes,flow"`
	Redirects    map[string]string  `yaml:"redirects,flow"`
	Services     map[string]Service `yaml:"services,flow"`
}

const configNameS string = "./config/config.yml"
const configNameL string = "./config/config.yaml"

// TODO: Add config validation

// ReadConfig read config.yaml and return is
func ReadConfig() (YamlConfig, error) {
	conf := YamlConfig{}
	var file []byte
	var err error
	file, err = ioutil.ReadFile(configNameS)
	if err != nil {
		file, err = ioutil.ReadFile(configNameL)
		if err != nil {
			return conf, err
		}
	}

	err = yaml.Unmarshal(file, &conf)
	if err != nil {
		return conf, err
	}

	return conf, nil
}
